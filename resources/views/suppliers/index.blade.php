<!-- resources/views/suppliers.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->
    <div class="panel-body width7 ct">
        <a class="btn btn-default" href="{{ url('new-supplier') }}">New Supplier</a>
        <br>
        <br>
        <!-- Display Validation Errors -->
        @include('common.errors')

        <table class="table table-hover">
          <!-- Table Headings -->
          <thead>
            <th width="20">&nbsp</th>
            <th>Supplier</th>
            <th>Category</th>
            <th>City</th>
            <th>Confirmed</th>
            <th width="200">Actions</th>
          </thead>

          <!-- Table Body -->
          <tbody>
              @foreach ($suppliers as $supplier)
                  <tr>
                      <td><img src="../../../../kurima/{{ ( $supplier->avator != '' ) ? $supplier->avator : 'uploads/avator.png' }}" style="width:20px;"/></td>
                      <!-- Supplier Name -->
                      <td class="table-text">
                          <div>{{ $supplier->name }}</div>
                      </td>

                      <td class="table-text">
                          <div>{{ $supplier->category }}</div>
                      </td>

                      <td class="table-text">
                          <div>{{ $supplier->city }}</div>
                      </td>

                      <td class="table-text">
                          <div>
                            @if( $supplier->confirm == 1 )
                              <div class="label label-success">Yes</div>
                            @elseif( $supplier->confirm == 0 )
                              <div class="label label-warning">No</div>
                            @endif
                          </div>
                      </td>

                      <td>
                          <!-- TODO: Delete Button -->
                          <div class="row">
                            <div class="col-md-6">
                              <a href="{{ url('new-supplier/'.$supplier->id) }}" type="submit" id="edit-task-{{ $supplier->id }}" class="btn btn-primary">
                                  <i class="fa fa-btn fa-pencil"></i>Edit
                              </a>
                            </div>
                            <div class="col-md-6">
                              <a href="{{ url('remove-supplier/'.$supplier->id) }}" type="submit" id="delete-task-{{ $supplier->id }}" class="btn btn-danger">
                                  <i class="fa fa-btn fa-trash"></i>Delete
                              </a>
                            </div>
                          </div>

                      </td>
                  </tr>
              @endforeach
          </tbody>
        </table>
    </div>

    <!-- TODO: Current Suppliers -->
@endsection
