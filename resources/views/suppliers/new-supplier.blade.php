<!-- resources/views/suppliers.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body width7 ct">
      <a class="btn btn-default" href="{{ url('/') }}">Back</a>
        <!-- Display Validation Errors -->
        @include('common.errors')

        <!-- New Supplier Form -->
        <form action="
        @if ( !isset($supplier) )
          {{ url('create-supplier') }}
        @else
          {{ url('edit-supplier/'. $supplier->id) }}
        @endif"
        method="POST" class="form-horizontal">
            {!! csrf_field() !!}


            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Full Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control"
                    value="@if(isset($supplier) && count($errors) == 0 ){{ $supplier->name }}@else{{ old('name') }}@endif">
                </div>
            </div>

            <div class="form-group">
                <label for="city" class="col-sm-3 control-label">City</label>
                <div class="col-sm-6">
                    <input type="text" name="city" id="city" class="form-control"
                    value="@if(isset($supplier) && count($errors) == 0 ){{ $supplier->city }}@else{{ old('city') }}@endif">
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                <div class="col-sm-6">
                    <input type="text" name="phone" id="phone" class="form-control"
                    value="@if(isset($supplier) && count($errors) == 0 ){{ $supplier->phone }}@else{{ old('phone') }}@endif">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="email" name="email" id="email" class="form-control"
                    value="@if(isset($supplier) && count($errors) == 0 ){{ $supplier->email }}@else{{ old('email') }}@endif">
                </div>
            </div>

            @if ( isset($supplier) )
            <input type="hidden" name="memberid" id="memberid" value="{{ $supplier->id }}">

            <div class="form-group">
                <label for="phone2" class="col-sm-3 control-label">Secondary Phone Number</label>
                <div class="col-sm-6">
                    <input type="text" name="phone2" id="phone2" class="form-control"
                    value="@if(isset($supplier) && count($errors) == 0 ){{ $supplier->phone2 }}@else{{ old('phone2') }}@endif">
                </div>
            </div>

            <div class="form-group">
                <label for="confirm" class="col-sm-3 control-label">Confirmed</label>
                <div class="col-sm-6">
                    <select name="confirm" id="confirm">
                      <option @if( $supplier->confirm == 0 ) selected @endif value="0">No</option>
                      <option @if( $supplier->confirm == 1 ) selected @endif value="1">Yes</option>
                    </select>
                </div>
            </div>

            @endif

            @if ( !isset($supplier) )

            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-6">
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>

            @endif

            <!-- Add Supplier Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-send"></i> Submit
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- TODO: Current Supplier -->
@endsection
