<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

  /**
   * Get all of the posts for the user.
   */
  public function posts()
  {
      return $this->hasMany(Post::class);
  }

  /**
   * Get all of the categories for the user.
   */
  public function categories()
  {
      return $this->belongsTo(Category::class);
  }


}
