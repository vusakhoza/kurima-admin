<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Member;
use App\Category;
use App\Http\Requests;

class SupplierController extends Controller
{

  /**
   * Create new supplier.
   *
   */
  public function destroy($id)
  {
    $member = Member::find($id);
    if( $member != null )
      $member->delete();

    return redirect('/');
  }

  /**
   * Create new supplier.
   *
   */
  public function edit(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|max:255',
      'city' => 'required|max:255',
      'phone' => 'required|max:255',
      'email' => 'required|email|max:255',
    ]);

    $fields = array('name', 'city', 'phone', 'phone2', 'email', 'confirm');
    $member = Member::find($request->memberid);
    //var_dump($member);
    foreach( $fields as $field ) {
      $member->$field = $request->$field;
    }

    if( $member->save() )
      return redirect('/');
  }

  /**
   * Create new supplier.
   *
   */
  public function store(Request $request)
  {
      $this->validate($request, [
        'name' => 'required|max:255',
        'city' => 'required|max:255',
        'phone' => 'required|max:255|unique:members',
        'email' => 'required|email|unique:members|max:255',
        'password' => 'required|max:12|min:6',
      ]);

      // Create The Supplier...
      $fields = array('name', 'city', 'phone', 'password', 'email');
      $member = new Member;
      foreach( $fields as $field ) {
        $member->$field = $request->$field;
      }

      $member->date = date('D M d Y G:i:s');
      $member->confirm = 0;
      $member->supplier = 1;

      if( $member->save() )
        return redirect('/');
  }

  /**
   * Send user to new supplier page.
   *
   */
  public function new_supplier( $id = null ) {
    $supplier = Member::find($id);
    //print_r( $supplier );
    return view('suppliers.new-supplier', ['supplier' => $supplier]);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
      $suppliers = Member::where('supplier', 1)->orderBy('date', 'desc')->get();

      foreach ($suppliers as $supplier) {
        $category = Category::find($supplier->category);
        if( $category != null )
          $supplier->category = $category->name;
        else
          $supplier->category = "None Assigned yet";


      }

      return view('suppliers.index', ['suppliers' => $suppliers]);
  }

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

}
