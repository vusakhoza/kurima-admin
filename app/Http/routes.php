<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Member;
use Illuminate\Http\Request;

//authorizer
Route::auth();

//default route
Route::get('/', 'SupplierController@index');

//create new member
Route::post('/edit-supplier/{id}', 'SupplierController@edit');

//create new member
Route::post('/create-supplier', 'SupplierController@store');

//direct user to create new member/edit existing member page depending on the presence of the id variable
Route::get('/new-supplier/{id?}', 'SupplierController@new_supplier');

//remove member
Route::get('/remove-supplier/{id}', 'SupplierController@destroy');
