<?php

//still under construction

namespace App\Repositories;

use App\Member;
use App\Category;

class SupplierRepository
{
    /**
     * Get the category name for a given member.
     *
     * @return Collection
     */
    public function categoryName(Member $member)
    {

      $category = Category::find($member->category);
      if( $category != null )
        $category = $category->name;
      else
        $category = "None Assigned yet";

      return $category;

    }
}
