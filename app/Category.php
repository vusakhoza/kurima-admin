<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /**
   * Get the member that owns the category.
   */
  public function members()
  {
      return $this->hasMany(Member::class, 'category');
  }

  /**
   * Define name of table.
   */
  protected $table = "categories";
}
