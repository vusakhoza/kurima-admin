<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
   * Get the member that owns the post.
   */
  public function member()
  {
      return $this->belongsTo(Member::class);
  }
}
